import http from './http';

function login(email: string, password: string): Promise<any> {
    // console.log(email)
    // console.log(password)
    return http.post('/auth/login', { email, password })
}

export default { login };
